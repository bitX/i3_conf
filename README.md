# Sample GitLab Project

This sample project shows how a project in GitLab looks for demonstration purposes. It contains issues, merge requests and Markdown files in many branches,
named and filled with lorem ipsum.

You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

[Learn more about creating GitLab projects.](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)

#############################################################################################################################################
************************************************************* i3-gaps config ***************************************************************
#############################################################################################################################################

i3-gaps is a fork of i3wm, a tiling window manager for X11. It is kept up to date with upstream, adding a few additional features such as gaps between windows (see below for a complete list).

![image](https://i.imgur.com/hW4Q9qJ.jpg)
![image](https://i.imgur.com/CP6Xfxn.png)
![image](https://i.imgur.com/6hJDa3D.png)
![image](https://i.imgur.com/W0datS9.png)
![image](https://i.imgur.com/YZMqix2.jpg)
![image](https://i.imgur.com/Gh6yXKG.jpg)
![image](https://i.imgur.com/fwyli9y.jpg)
![image](https://i.imgur.com/LxHdQe1.jpg)
![image](https://i.imgur.com/vofSjRI.jpg)
![image](https://i.imgur.com/EVbhbNR.png)
